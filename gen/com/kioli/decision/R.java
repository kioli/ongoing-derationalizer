/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.kioli.decision;

public final class R {
    public static final class attr {
    }
    public static final class color {
        public static final int black=0x7f040001;
        public static final int button_bottom_gradient=0x7f040008;
        /**  Button 
         */
        public static final int button_top_gradient=0x7f040007;
        public static final int green=0x7f040004;
        public static final int light_grey=0x7f040003;
        public static final int post_message_gray=0x7f040006;
        public static final int transparent=0x7f040002;
        public static final int username_blue=0x7f040005;
        public static final int white=0x7f040000;
    }
    public static final class dimen {
        /**  Font 
         */
        public static final int app_font=0x7f050008;
        public static final int header_heigth=0x7f050005;
        /**  Login 
         */
        public static final int height_login_btn=0x7f050006;
        /**  Question 
         */
        public static final int question_max_height=0x7f050007;
        public static final int spacing_large=0x7f050002;
        public static final int spacing_medium=0x7f050001;
        /**  General 
         */
        public static final int spacing_small=0x7f050000;
        public static final int spacing_xlarge=0x7f050003;
        public static final int spacing_xxlarge=0x7f050004;
    }
    public static final class drawable {
        public static final int add_acct_titlebar=0x7f020000;
        public static final int add_btn_clicked=0x7f020001;
        public static final int add_btn_not_clicked=0x7f020002;
        public static final int add_button=0x7f020003;
        public static final int add_people_btn=0x7f020004;
        public static final int add_people_clicked=0x7f020005;
        public static final int add_people_not_clicked=0x7f020006;
        public static final int arrow=0x7f020007;
        public static final int background=0x7f020008;
        public static final int btn_question=0x7f020009;
        public static final int btn_question_not_pressed=0x7f02000a;
        public static final int btn_question_pressed=0x7f02000b;
        public static final int close_btn=0x7f02000c;
        public static final int close_btn_clicked=0x7f02000d;
        public static final int close_btn_not_clicked=0x7f02000e;
        public static final int ic_action_search=0x7f02000f;
        public static final int icon=0x7f020010;
        public static final int login_button=0x7f020011;
        public static final int login_button_clicked=0x7f020012;
        public static final int login_button_not_clicked=0x7f020013;
        public static final int login_textfields=0x7f020014;
        public static final int logout_btn=0x7f020015;
        public static final int logout_btn_clicked=0x7f020016;
        public static final int logout_btn_not_clicked=0x7f020017;
        public static final int message_board_titlebar=0x7f020018;
        public static final int new_message_btn=0x7f020019;
        public static final int new_message_clicked=0x7f02001a;
        public static final int new_message_not_clicked=0x7f02001b;
        public static final int newmess_titlebar=0x7f02001c;
        public static final int post_btn=0x7f02001d;
        public static final int post_btn_clicked=0x7f02001e;
        public static final int post_btn_not_clicked=0x7f02001f;
        public static final int register_button=0x7f020020;
        public static final int register_button_clicked=0x7f020021;
        public static final int register_button_not_clicked=0x7f020022;
        public static final int rounded_corners_white=0x7f020023;
        public static final int settings_btn_clicked=0x7f020024;
        public static final int settings_btn_not_clicked=0x7f020025;
        public static final int username_textbox=0x7f020026;
    }
    public static final class id {
        public static final int add_account_close_btn=0x7f080001;
        public static final int add_account_header=0x7f080000;
        public static final int add_acct_add_btn=0x7f080002;
        public static final int add_acct_email_textbox=0x7f080004;
        public static final int add_acct_pass_textbox=0x7f080005;
        public static final int add_acct_user_textbox=0x7f080003;
        public static final int add_follow_button_id=0x7f080019;
        public static final int answer1=0x7f080009;
        public static final int answer1_title=0x7f080008;
        public static final int answer2=0x7f08000b;
        public static final int answer2_title=0x7f08000a;
        public static final int btn_answer1=0x7f080024;
        public static final int btn_answer2=0x7f080025;
        public static final int btn_confirm=0x7f08000c;
        public static final int btn_confirm_settings=0x7f080013;
        public static final int cancel_button_dialog_id=0x7f08001e;
        public static final int closebutton=0x7f080020;
        public static final int imageView1=0x7f08001c;
        public static final int layout_root=0x7f08001b;
        public static final int linearLayout1=0x7f080017;
        public static final int linear_layout_posts=0x7f080016;
        public static final int login_button_id=0x7f080010;
        public static final int login_email=0x7f08000d;
        public static final int login_password=0x7f08000e;
        public static final int logout_button_id=0x7f08001a;
        public static final int new_message_button_id=0x7f080018;
        public static final int ok_button_dialog_id=0x7f08001f;
        public static final int pitch_seekbar=0x7f080012;
        public static final int postText=0x7f080022;
        public static final int postbutton=0x7f080021;
        public static final int question=0x7f080007;
        public static final int question_shown=0x7f080023;
        public static final int question_title=0x7f080006;
        public static final int register_button_id=0x7f08000f;
        public static final int relativeLayout1=0x7f080014;
        public static final int scrollView1=0x7f080015;
        public static final int search_query=0x7f08001d;
        public static final int setting_pitch_title=0x7f080011;
    }
    public static final class layout {
        public static final int add_account=0x7f030000;
        public static final int home=0x7f030001;
        public static final int login=0x7f030002;
        public static final int menu_settings=0x7f030003;
        public static final int message_board=0x7f030004;
        public static final int new_follow_dialog=0x7f030005;
        public static final int new_message=0x7f030006;
        public static final int questions=0x7f030007;
    }
    public static final class string {
        /**  Database 
         */
        public static final int ContentProviderUnknownURI=0x7f060002;
        public static final int ContentProviderUnsupportedURI=0x7f060003;
        public static final int answer1_hint=0x7f060007;
        public static final int answer1_title=0x7f060006;
        public static final int answer2_hint=0x7f060009;
        public static final int answer2_title=0x7f060008;
        public static final int app_name=0x7f060000;
        public static final int button_confirm=0x7f06000a;
        public static final int email_title=0x7f06000c;
        public static final int error_lang_initialization=0x7f060012;
        public static final int error_lang_unsupported=0x7f060011;
        public static final int menu_settings=0x7f060001;
        public static final int name_please_dialog_string=0x7f06000e;
        /**  New Message 
         */
        public static final int newMessage_hint=0x7f060010;
        public static final int new_follow_dialog_user=0x7f06000f;
        public static final int password_title=0x7f06000d;
        public static final int question_hint=0x7f060005;
        /**  Question 
         */
        public static final int question_title=0x7f060004;
        public static final int settings_pitch_title=0x7f060013;
        public static final int settings_speed_title=0x7f060014;
        /**  Add User 
         */
        public static final int user_title=0x7f06000b;
    }
    public static final class style {
        public static final int AppTheme=0x7f070000;
        public static final int AppTheme2=0x7f070001;
        public static final int Button=0x7f070002;
    }
}
