package com.kioli.decision;

import java.util.ArrayList;
import java.util.Locale;
import org.usergrid.android.client.response.ApiResponse;
import android.app.Activity;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.kioli.decision.apigee.messagee.MessageBoard;
import com.kioli.decision.apigee.messagee.Messagee;
import com.kioli.decision.apigee.messagee.NewMessage;
import com.kioli.decision.apigee.model.Question;
import com.kioli.decision.database.tables.Questions.QuestionsAll;
import com.kioli.decision.utils.Constants;

public class Questions extends Activity implements TextToSpeech.OnInitListener {

	private TextToSpeech tts;
	private Float pitch;
	private ArrayList<Question> questions_total;
	private ArrayList<Question> questions_chosen;
	private int limit = 5;
	private MyWaitCount counterWait;
	private MyQuestionCount counterQuestion;
	private int countdownWaiting = 3000;
	private int countdownInterval = 1000;
	private int countdownQuestions = 1000;
	private ProgressDialog waitProgressDialog = null;
	private ProgressDialog loginPostingDialog = null;
	private TextView quest;
	private Button btn1;
	private Button btn2;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.questions);
		getQuestion();
		init();
	}

	@Override
	public void onDestroy() {
		if (tts != null) {
			tts.stop();
			tts.shutdown();
		}
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();
		SharedPreferences pref = getSharedPreferences(Constants.SHRD_PRF, Activity.MODE_PRIVATE);
		pitch = ((float) pref.getInt(Constants.SHRD_PRF_PITCH, 10)) / 10;
		tts.setPitch(pitch);
		showProgressDialog();
	}

	// dialog to display waiting progress
	public void showProgressDialog() {
		counterWait.start();
		waitProgressDialog = ProgressDialog.show(this, "", "Preparing questions", true);
	}

	// hide waiting dialog
	public void hideProgressDialog() {
		if (waitProgressDialog != null) {
			waitProgressDialog.dismiss();
		}
		waitProgressDialog = null;
		nextQuestion();
	}

	// create dialog to show progress wheel
	public void showPostProgressDialog() {
		loginPostingDialog = ProgressDialog.show(this, "", "Posting. Please wait...", true);
	}

	// hide post progress dialog
	public void hidePostProgressDialog() {
		if (loginPostingDialog != null) {
			loginPostingDialog.dismiss();
		}
		loginPostingDialog = null;
	}

	private void init() {
		tts = new TextToSpeech(this, this);
		counterWait = new MyWaitCount(countdownWaiting, countdownInterval);
		counterQuestion = new MyQuestionCount(countdownQuestions, countdownInterval);
		quest = (TextView) findViewById(R.id.question_shown);
		btn1 = (Button) findViewById(R.id.btn_answer1);
		btn2 = (Button) findViewById(R.id.btn_answer2);
		quest.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/GoodDog.otf"));
		btn1.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Molot.otf"));
		btn2.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Molot.otf"));
		btn1.setOnClickListener(listener);
		btn2.setOnClickListener(listener);
	}

	private void getQuestion() {
		questions_total = new ArrayList<Question>();
		questions_chosen = new ArrayList<Question>();

		Cursor cursor = this.getContentResolver().query(QuestionsAll.TABLE_URI,
				new String[] { QuestionsAll._ID, QuestionsAll.QUESTION, QuestionsAll.ANSWER1, QuestionsAll.ANSWER2 }, null, null, "RANDOM()");

		while (cursor.moveToNext()) {
			String question = cursor.getString(cursor.getColumnIndex(QuestionsAll.QUESTION));
			String answer1 = cursor.getString(cursor.getColumnIndex(QuestionsAll.ANSWER1));
			String answer2 = cursor.getString(cursor.getColumnIndex(QuestionsAll.ANSWER2));
			questions_total.add(new Question(question, answer1, answer2));
		}

		for (int i = 0; i < limit; i++) {
			questions_chosen.add(questions_total.get(i));
		}

		Bundle b = getIntent().getExtras();
		Question obj = b.getParcelable(Constants.PARCELABLE_INTENT);
		questions_chosen.add(new Question(obj.getQuestion(), obj.getAnswer1(), obj.getAnswer2()));
	}

	private void setQuestion() {
		Question question = questions_chosen.remove(0);
		quest.setText(question.getQuestion());
		btn1.setText(question.getAnswer1());
		btn2.setText(question.getAnswer2());
	}

	private void nextQuestion() {
		setQuestion();
		speak();
	}

	public void onInit(int status) {
		if (status == TextToSpeech.SUCCESS) {
			int result = tts.setLanguage(Locale.getDefault());
			if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
				Toast.makeText(Questions.this, getResources().getString(R.string.error_lang_unsupported), Toast.LENGTH_LONG).show();
			}
		} else {
			Toast.makeText(Questions.this, getResources().getString(R.string.error_lang_unsupported), Toast.LENGTH_LONG).show();
		}
	}

	private void speak() {
		counterQuestion.start();
		tts.speak(quest.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
	}

	public class MyWaitCount extends CountDownTimer {
		public MyWaitCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onFinish() {
			hideProgressDialog();
		}

		@Override
		public void onTick(long millisUntilFinished) {
		}
	}

	public class MyQuestionCount extends CountDownTimer {
		public MyQuestionCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onFinish() {
			if (tts.isSpeaking()) {
				counterQuestion.start();
			} else {
				Handler myHandler = new Handler();
				myHandler.postDelayed(myRunnable, 500);
			}
		}

		@Override
		public void onTick(long millisUntilFinished) {
		}
	}

	private Runnable myRunnable = new Runnable() {
		public void run() {
			nextQuestion();
		}
	};

	private OnClickListener listener = new OnClickListener() {
		public void onClick(View v) {
			counterQuestion.cancel();
			if (questions_chosen.isEmpty()) {
				Button b = (Button) v;
				showPostProgressDialog();
				new PostTask(getApplication(), quest.getText().toString().trim() + " " + b.getText().toString()).execute();
			} else {
				Handler myHandler = new Handler();
				myHandler.postDelayed(myRunnable, 500);
			}
		}
	};

	// Thread for sending post through client
	private class PostTask extends AsyncTask<Void, Void, ApiResponse> {

		Application app;
		String message;

		public PostTask(Application app, String message) {
			this.app = app;
			this.message = message;
		}

		// main thread function for communicating through client to post message
		protected ApiResponse doInBackground(Void... v) {
			return ((Messagee) app).messController.post(message);
		}

		// on communication done check for errors and return to message board
		protected void onPostExecute(ApiResponse response) {
			hidePostProgressDialog();

			// // show error if reply is empty or contains error
			// if ((response == null) || "invalid_grant".equals(response.getError())) {
			// showPostError();
			// } else {
			startActivity(new Intent(Questions.this, MessageBoard.class));
			finish();
			// }
		}
	}
}