package com.kioli.decision.apigee.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Question implements Parcelable {

	String question;
	String answer1;
	String answer2;

	/** Standard basic constructors for non-parcel object creation */
	public Question() {
		question = "";
		answer1 = "";
		answer2 = "";
	}
	
	public Question(String q, String a1, String a2) {
		question = q;
		answer1 = a1;
		answer2 = a2;
	}

	/** Constructor to use when re-constructing object from a parcel *
	 * 
	 * @param in
	 *        a parcel from which to read this object */
	public Question(Parcel in) {
		readFromParcel(in);
	}
	
	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer1() {
		return answer1;
	}

	public void setAnswer1(String answer1) {
		this.answer1 = answer1;
	}

	public String getAnswer2() {
		return answer2;
	}

	public void setAnswer2(String answer2) {
		this.answer2 = answer2;
	}

	public int describeContents() {
		return 0;
	}

	/** Writes the data in the parcel and when read they will come back in the same order */
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(question);
		dest.writeString(answer1);
		dest.writeString(answer2);
	}

	/** Called from the constructor to create this object from a parcel.
	 * 
	 * @param in
	 *        parcel from which to re-create object */
	private void readFromParcel(Parcel in) {
		question = in.readString();
		answer1 = in.readString();
		answer2 = in.readString();
	}

	/** This field is needed for Android to be able to create new objects, individually or as arrays. This also means
	 * that you can use use the default constructor to create the object. */
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public Question createFromParcel(Parcel in) {
			return new Question(in);
		}

		public Question[] newArray(int size) {
			return new Question[size];
		}
	};
}