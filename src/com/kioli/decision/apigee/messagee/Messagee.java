//Messagee.java - Global class for creating a single message controller for
//views to use for communications. 

package com.kioli.decision.apigee.messagee;

import android.app.Application;
import com.kioli.decision.apigee.controller.MessageController;

public class Messagee extends Application {
	public MessageController messController = new MessageController();
}