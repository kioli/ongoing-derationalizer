//NewMessage.java - Handles view for creating a new post.

package com.kioli.decision.apigee.messagee;

import org.usergrid.android.client.response.ApiResponse;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import com.kioli.decision.R;

public class NewMessage extends Activity implements View.OnClickListener {

	ProgressDialog loginProgressDialog = null;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_message);

		findViewById(R.id.closebutton).setOnClickListener(this);
		findViewById(R.id.postbutton).setOnClickListener(this);
	}

	public void onClick(View arg0) {
		switch (arg0.getId()) {
			case R.id.closebutton:
				startActivity(new Intent(NewMessage.this, MessageBoard.class));
				finish();
				break;
			case R.id.postbutton:
				String message = ((EditText) findViewById(R.id.postText)).getText().toString().trim();
				showPostProgress();
				new PostTask(this.getApplication(), message).execute();
				break;
		}
	}

	// create dialog to show progress wheel
	public void showPostProgress() {
		loginProgressDialog = ProgressDialog.show(this, "", "Posting. Please wait...", true);
	}

	// hide post progress dialog
	public void hidePostProgressDialog() {
		if (loginProgressDialog != null) {
			loginProgressDialog.dismiss();
		}
		loginProgressDialog = null;
	}

	// show error dialog
	public void showPostError() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Unable to post. Please try again.").setCancelable(false).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	// Thread for sending post through client
	private class PostTask extends AsyncTask<Void, Void, ApiResponse> {

		Application app;
		String message;

		public PostTask(Application app, String message) {
			this.app = app;
			this.message = message;
		}

		// main thread function for communicating through client to post message
		protected ApiResponse doInBackground(Void... v) {
			return ((Messagee) app).messController.post(message);
		}

		// on communication done check for errors and return to message board
		protected void onPostExecute(ApiResponse response) {
			hidePostProgressDialog();

			// show error if reply is empty or contains error
			if ((response == null) || "invalid_grant".equals(response.getError())) {
				showPostError();
			} else {
				startActivity(new Intent(NewMessage.this, MessageBoard.class));
				finish();
			}
		}
	}
}