//Login.java - handles login view

package com.kioli.decision.apigee.messagee;

import org.usergrid.android.client.response.ApiResponse;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import com.kioli.decision.Home;
import com.kioli.decision.R;
import com.kioli.decision.utils.Constants;

public class Login extends Activity implements View.OnClickListener {

	ProgressDialog loginProgressDialog = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		findViewById(R.id.login_button_id).setOnClickListener(this);
		findViewById(R.id.register_button_id).setOnClickListener(this);
	}

	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.login_button_id:
				String email = ((EditText) findViewById(R.id.login_email)).getText().toString().trim();
				String password = ((EditText) findViewById(R.id.login_password)).getText().toString().trim();
				showLoginProgress();
				new LoginTask(email, password, this.getApplication()).execute();
				break;
			case R.id.register_button_id:
				startActivityForResult(new Intent(Login.this, AddAccount.class), 0);
				break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK && data.hasExtra(Constants.add_account_name)) {
			((EditText) findViewById(R.id.login_email)).setText(data.getStringExtra(Constants.add_account_name));
			((EditText) findViewById(R.id.login_password)).requestFocus();
		}
	}

	// dialog to display login progress
	public void showLoginProgress() {
		loginProgressDialog = ProgressDialog.show(this, "", "Signing in. Please wait...", true);
	}

	// hide logging in dialog
	public void hideLoginProgressDialog() {
		if (loginProgressDialog != null) {
			loginProgressDialog.dismiss();
		}
		loginProgressDialog = null;
	}

	// show login error dialog
	public void showLoginError() {
		((EditText) findViewById(R.id.login_password)).getText().clear();
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Unable to sign in. Please check your name, password, and API URL.").setCancelable(false)
				.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	// thread to attempt login
	private class LoginTask extends AsyncTask<Void, Void, ApiResponse> {

		String email;
		String password;
		Application app;

		public LoginTask(String email, String password, Application app) {
			this.email = email;
			this.password = password;
			this.app = app;
		}

		// main thread function attempts login
		protected ApiResponse doInBackground(Void... v) {
			// clear all posts in case previous user data is still active
			((Messagee) app).messController.getPosts().clearAll();
			((Messagee) app).messController.getPostImages().clearImages();
			return ((Messagee) app).messController.login(email, password);
		}

		// check if login was successful and act on it
		protected void onPostExecute(ApiResponse response) {
			if ((response == null) || "invalid_grant".equals(response.getError())) {
				hideLoginProgressDialog();
				showLoginError();
			} else {
				// get posts for user
				((Messagee) app).messController.getPostsFromClient();
				startActivity(new Intent(Login.this, Home.class));
				hideLoginProgressDialog();
				finish();
			}
		}
	}
}