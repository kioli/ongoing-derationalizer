//AddAccount.java - handles view for creating new accounts

package com.kioli.decision.apigee.messagee;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.usergrid.android.client.response.ApiResponse;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.kioli.decision.R;
import com.kioli.decision.utils.Constants;

public class AddAccount extends Activity {

	private enum error_types {GENERAL, CLIENT, PASSWORD, EMAIL_LENGTH, EMAIL_FORMAT, SUCCESS}
	private error_types err;
	private ProgressDialog addAccountProgressDialog = null;
	private String username;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_account);

		findViewById(R.id.add_acct_add_btn).setOnClickListener(listener);
		findViewById(R.id.add_account_close_btn).setOnClickListener(listener);
	}

	private OnClickListener listener = new OnClickListener() {
		
		public void onClick(View v) {
			switch (v.getId()) {
				case R.id.add_account_close_btn:
					startActivity(new Intent(AddAccount.this, Login.class));
					setResult(RESULT_CANCELED);
					finish();
					break;
				case R.id.add_acct_add_btn:
					username = ((EditText) findViewById(R.id.add_acct_user_textbox)).getText().toString().trim();
					String email = ((EditText) findViewById(R.id.add_acct_email_textbox)).getText().toString().trim();
					String password = ((EditText) findViewById(R.id.add_acct_pass_textbox)).getText().toString().trim();

					// if length of email < 6, show error
					if (email.length() < 6) {
						showAddAccountEmailLengthError();
						break;
					}

					// if email does not look like email show error
					Pattern pattern = Pattern.compile(".+@.+\\.[a-z]+");
					Matcher matcher = pattern.matcher(email);
					boolean matchFound = matcher.matches();
					if (!matchFound) {
						showAddAccountEmailFormatError();
						break;
					}

					// if length password < 5, show error
					if (password.length() < 5) {
						showAddAccountPasswordLengthError();
						break;
					}

					// try to create account
					showAddAcctProgress();
					new AddAcctTask(AddAccount.this.getApplication(), username, password, email).execute();
					break;
			}
		}
	};
	
	// show add account progress dialog
	public void showAddAcctProgress() {
		addAccountProgressDialog = ProgressDialog.show(this, "", "Creating Account. Please wait...", true);
	}

	// remove add account progress dialog
	public void hideAddAcctProgressDialog() {
		if (addAccountProgressDialog != null) {
			addAccountProgressDialog.dismiss();
		}
		addAccountProgressDialog = null;
	}

	// add account error dialog
	private void showAddAccountError() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Unable to add account. Please try again.").setCancelable(false)
				.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	// client error dialog
	private void showAddAccountClientError(String error) {
		String message = "Error: " + error;
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message).setCancelable(false).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	// password length error
	private void showAddAccountPasswordLengthError() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Password must be five characters or more. Please try again.").setCancelable(false)
				.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	// email length error
	private void showAddAccountEmailLengthError() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Email must be six characters or more. Please try again.").setCancelable(false)
				.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	// email length error
	private void showAddAccountEmailFormatError() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Email format error. eg. ui@jquery.com").setCancelable(false)
				.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	// show account created dialog
	private void showAddAccountSuccess() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("New account created.").setCancelable(false).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
				Intent intent = new Intent();
				intent.putExtra(Constants.add_account_name, username);
				setResult(RESULT_OK, intent);
				finish();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	// thread to try to add account
	private class AddAcctTask extends AsyncTask<Void, Void, ApiResponse> {

		Application app;
		String username;
		String password;
		String email;

		public AddAcctTask(Application app, String username, String password, String email) {
			this.app = app;
			this.username = username;
			this.password = password;
			this.email = email;
		}

		// main function, attempts to add account by post
		protected ApiResponse doInBackground(Void... v) {
			return ((Messagee) app).messController.addAccount(username, password, email);
		}

		// client responded, check for errors, if none show success dialog
		protected void onPostExecute(ApiResponse response) {
			hideAddAcctProgressDialog();

			// if there is no response show error dialog
			if (response == null) {
				showAddAccountError();
			} else if (response.getError() != null) {
				showAddAccountClientError(response.getError().toString());
			} else {
				showAddAccountSuccess();
			}
		}
	}
}