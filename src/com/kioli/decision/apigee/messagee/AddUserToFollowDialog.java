//AddUserToFollowDialog - View for adding users to follow

package com.kioli.decision.apigee.messagee;

import org.usergrid.android.client.response.ApiResponse;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.EditText;
import com.kioli.decision.R;

public class AddUserToFollowDialog extends Activity {

	private EditText nameTextField;
	private ProgressDialog addFollowDialog = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_follow_dialog);

		// grab handle to text field
		nameTextField = (EditText) findViewById(R.id.search_query);

		// blur background
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND, WindowManager.LayoutParams.FLAG_DIM_BEHIND);

		// set ok button click listener
		findViewById(R.id.ok_button_dialog_id).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				showAddFollowProgress();
				new AddFollowTask(getApplication(), nameTextField.getText().toString().trim()).execute();
			}
		});

		// set cancel button listener
		findViewById(R.id.cancel_button_dialog_id).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				startActivity(new Intent(AddUserToFollowDialog.this, MessageBoard.class));
				finish();
			}
		});
	}

	// show adding user dialog
	public void showAddFollowProgress() {
		addFollowDialog = ProgressDialog.show(this, "", "Adding user. Please wait...", true);
	}

	// hide adding user dialog
	public void hideAddFollowProgressDialog() {
		if (addFollowDialog != null) {
			addFollowDialog.dismiss();
		}
		addFollowDialog = null;
	}

	// show adding user error dialog
	public void showAddFollowError() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Unable to add user. Please try again.").setCancelable(false)
				.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	// show user not found error dialog
	public void showNoFollowError() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("User not found. Please try again.").setCancelable(false).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	// add user to follow thread
	private class AddFollowTask extends AsyncTask<Void, Void, ApiResponse> {

		String username;
		Application app;

		public AddFollowTask(Application app, String followName) {
			super();
			this.app = app;
			this.username = followName;
		}

		// main function to try and add user to follow
		@Override
		protected ApiResponse doInBackground(Void... v) {
			return ((Messagee) app).messController.addFollow(username);
		}

		// add user request complete, check for errors, if none return to messageboard
		@Override
		protected void onPostExecute(ApiResponse response) {
			hideAddFollowProgressDialog();

			// show error if no response or response with error
			if ((response == null) || "invalid_grant".equals(response.getError())) {
				showAddFollowError();
			} else if (response.getEntityCount() == 0) {
				showNoFollowError();
			} else {
				startActivity(new Intent(AddUserToFollowDialog.this, MessageBoard.class));
				finish();
			}
		}
	}
}