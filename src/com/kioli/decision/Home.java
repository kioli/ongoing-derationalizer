package com.kioli.decision;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.kioli.decision.apigee.model.Question;
import com.kioli.decision.utils.Constants;

public class Home extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);
		init();
	}

	private void init() {
		findViewById(R.id.btn_confirm).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				insertQuestion();
			}
		});
	}
	
	private void insertQuestion() {
		String question = ((EditText) findViewById(R.id.question)).getText().toString().trim();
		String answer1 = ((EditText) findViewById(R.id.answer1)).getText().toString().trim();
		String answer2 = ((EditText) findViewById(R.id.answer2)).getText().toString().trim();
		
		// Create a parcelable object
		Question obj = new Question();
		obj.setQuestion(question);
		obj.setAnswer1(answer1);
		obj.setAnswer2(answer2);
		
		Intent intent = new Intent(Home.this, Questions.class);
		intent.putExtra(Constants.PARCELABLE_INTENT, obj);
		startActivity(intent);
	}
}