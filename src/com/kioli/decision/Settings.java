package com.kioli.decision;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;
import com.kioli.decision.utils.Constants;

public class Settings extends Activity {

	SeekBar pitch_bar;
	Button button_confirm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_settings);
		init();
	}

	private void init() {
		pitch_bar = (SeekBar) findViewById(R.id.pitch_seekbar);
		getPreviousLevel();

		button_confirm = (Button) findViewById(R.id.btn_confirm_settings);
		button_confirm.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				SharedPreferences pref = getSharedPreferences(Constants.SHRD_PRF, Activity.MODE_PRIVATE);
				Editor prefsEditor = pref.edit();
				prefsEditor.putInt(Constants.SHRD_PRF_PITCH, pitch_bar.getProgress());
				prefsEditor.commit();
				finish();
			}
		});
	}

	private void getPreviousLevel() {
		SharedPreferences pref = getSharedPreferences(Constants.SHRD_PRF, Activity.MODE_PRIVATE);
		pitch_bar.setProgress(pref.getInt(Constants.SHRD_PRF_PITCH, 10));
	}
}