package com.kioli.decision.database;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.kioli.decision.utils.MyLog;

/** Class that creates the DataBase starting from the queries written in the files present in the folder assets
 * @author kioli */

public class DBHelper extends SQLiteOpenHelper {

	private static String DB_NAME;
	Context context;
	static int DB_VERSION = 1;
	private static DBHelper instance;

	private DBHelper(Context context, String databaseName) {
		super(context, databaseName, null, DB_VERSION);
		this.context = context;
		instance = this;
	}

	/** Return the instance of DBHelper */
	public static DBHelper getInstance(Context context) {

		DB_NAME = context.getPackageName().replace(".", "");
		if (instance == null)
			instance = new DBHelper(context, DB_NAME);
		return instance;
	}

	/** Creates the database using the queries stored in the folder assets/create */
	@Override
	public void onCreate(SQLiteDatabase db) {

		AssetManager assetManager = context.getAssets();
		String files[] = null;
		BufferedReader reader;
		String line;

		try {
			files = assetManager.list("sql/create");
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		if (files != null && files.length > 0) {
			db.beginTransaction();
			for (String filename : files) {
				try {
					reader = new BufferedReader(new InputStreamReader(assetManager.open("sql/create/" + filename)));

					while ((line = reader.readLine()) != null) {
						if (line.compareTo("") != 0 && line.length() > 1) {
							db.execSQL(line);
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			db.setTransactionSuccessful();
			db.endTransaction();
		}
		MyLog.e("DATABASE", "database created");
	}

	/** Upgrades the database using the queries stored in the folder assets/update */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		MyLog.e("DATABASE", "Upgrading database from version " + oldVersion + " to " + newVersion);

		AssetManager assetManager = context.getAssets();
		String files[] = null;
		BufferedReader reader;
		String line;

		try {
			files = assetManager.list("sql/upgrade");
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		if (files != null && files.length > 0) {
			db.beginTransaction();
			for (String filename : files) {
				try {
					reader = new BufferedReader(new InputStreamReader(assetManager.open("sql/upgrade/" + filename)));

					while ((line = reader.readLine()) != null) {
						if (line.compareTo("") != 0) {
							db.execSQL(line);
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			db.setTransactionSuccessful();
			db.endTransaction();
		}
		onCreate(db);
	}

	/** Method used to update the database when it has to be synchronized with the server
	 * @param sql */
	public void updateDB(String[] sql) {
		SQLiteDatabase db = getWritableDatabase();

		if (sql != null && sql.length > 0) {
			db.beginTransaction();
			for (String sqlSCommand : sql) {
				db.execSQL(sqlSCommand);
			}
			db.setTransactionSuccessful();
			db.endTransaction();
		}
		MyLog.e("DATABASE", "database updated");
	}
}
