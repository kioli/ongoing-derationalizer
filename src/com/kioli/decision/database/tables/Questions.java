package com.kioli.decision.database.tables;

import com.kioli.decision.database.myContentProvider;
import android.net.Uri;
import android.provider.BaseColumns;

public class Questions implements BaseColumns {

	public Questions() {
	}

	public static final class QuestionsAll implements BaseColumns {

		public static final String TABLE_NAME = "questions";
		public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.kioli.questions";
		public static final Uri TABLE_URI = Uri.parse("content://" + myContentProvider.AUTHORITY + "/" + TABLE_NAME);

		//public static final String ID = "AS _id";
		public static final String QUESTION = "question";
		public static final String ANSWER1 = "answer1";
		public static final String ANSWER2 = "answer2";
	}
}