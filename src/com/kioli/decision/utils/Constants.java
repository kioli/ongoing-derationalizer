package com.kioli.decision.utils;

public class Constants {
	
	public final static String add_account_name = "ACCOUNT_NAME";
	
	/** SharedPreferences */
	public final static String SHRD_PRF = "kioli.sharedPreferences" ;
	public final static String SHRD_PRF_PITCH = "pitch";
	
	/** Tables */
	public final static int DB_TABLE_QUESTIONS = 0;
	
	/** PARCELABLE INTENT LABLE*/
	public final static String PARCELABLE_INTENT = "parcelable";
}